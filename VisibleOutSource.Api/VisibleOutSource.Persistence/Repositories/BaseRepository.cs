﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Persistence.Repositories
{
    public class BaseRepository<T> : IGenericRepository<T> where T : class
    {
        public readonly DbContext _context;
        public readonly DbSet<T> _dbSet;

        public DbSet<T> Set { get; private set; }
        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
            Set = _dbSet;
        }

        public IOperationResult<T> Create(T entity)
        {
            _context.Add(entity);

            return BasicOperationResult<T>.Ok(entity);
        }

        public Task<List<T>> FindAllAsync()
        {
            IQueryable<T> queryable = _dbSet.AsQueryable();

            //foreach (Expression<Func<T, object>> include in includes)
            //{
            //    queryable = queryable.Include(include);
            //}

            return queryable.ToListAsync();
        }

        public Task SaveAsync() => _context.SaveChangesAsync();
    }
}
