﻿using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Persistence.Repositories
{
    public class OperationsLogRepository : BaseRepository<OperationsLog>, IOperationsLogRepository
    {
        private readonly ApplicationDbContext _context;

        public OperationsLogRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
