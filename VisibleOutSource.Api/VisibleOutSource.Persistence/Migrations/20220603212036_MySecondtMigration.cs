﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VisibleOutSource.Persistence.Migrations
{
    public partial class MySecondtMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Request",
                table: "OperationsLog");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Request",
                table: "OperationsLog",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
