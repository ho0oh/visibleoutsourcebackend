﻿using Microsoft.EntityFrameworkCore;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OperationsLog>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.ToTable(name: "OperationsLog");
            });

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<OperationsLog> OperationsLogs { get; set; }
    }
}
