﻿using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Core.Managers
{
    public class OperationsLogsManager
    {
        private readonly IOperationsLogRepository _operationsLogRepository;

        public OperationsLogsManager(IOperationsLogRepository operationsLogRepository)
        {
            _operationsLogRepository = operationsLogRepository;
        }

        public async Task SaveLogError(string errorMessage, string errorStackTrace)
        {
            try
            {
                OperationsLog operationsLog = BuildOperationsLog(errorMessage, errorStackTrace);
                _operationsLogRepository.Create(operationsLog);
                await _operationsLogRepository.SaveAsync();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Ha Ocurrido un error guardando datos enlog de base de datos.");
            }
        }

        private OperationsLog BuildOperationsLog(string errorMessage, string errorStackTrace)
        {

            return new OperationsLog
            {
                ErrorMessage = errorMessage,
                ErrorStackTrace = errorStackTrace,
                Date = DateTime.Now
            };

        }

        public async Task<IOperationResult<List<OperationsLog>>> GetData()
        {
            try
            {
                List<OperationsLog> uploadedDocuments = await _operationsLogRepository.FindAllAsync();

                return BasicOperationResult<List<OperationsLog>>.Ok(uploadedDocuments);

            }
            catch (Exception ex)
            {
                return BasicOperationResult<List<OperationsLog>>.Fail("Ha ocurrido un error al consultar la base de datos.", ex.ToString());
            }
        }
    }
}
