﻿using System.Net;
using System.Text.Json;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Core.Managers
{
    public class PostManager
    {
        private readonly IHttpServices _httpServices;
        const string urlHttpClient = "https://jsonplaceholder.typicode.com";
        const string urlObjectClient = "posts";

        public PostManager(IHttpServices httpServices)
        {
            _httpServices = httpServices;
        }

        public async Task<IOperationResult<Post>> GetDataPostById(int id)
        {
            try
            {
                var postResponde = await _httpServices.GetItem(urlHttpClient, urlObjectClient, id);

                if (!postResponde.IsSuccessful)
                {
                    if (postResponde.StatusCode == HttpStatusCode.NotFound)
                    {
                        return BasicOperationResult<Post>.Fail("No existe una publicación con este Id.");
                    }
                    return BasicOperationResult<Post>.Fail("Ha ocurrido un error al consultar la publicación.");
                }

                Post post = JsonSerializer.Deserialize<Post>(postResponde.Content.ToString());

                return BasicOperationResult<Post>.Ok(post);

            }
            catch (Exception ex)
            {
                return BasicOperationResult<Post>.Fail("Ha ocurrido un error al consultar la publicación.", ex.ToString());
            }
        }

        public async Task<IOperationResult<IEnumerable<Post>>> GetDataAllPosts()
        {
            try
            {
                var postResponde = await _httpServices.GetItems(urlHttpClient, urlObjectClient);

                if (!postResponde.IsSuccessful)
                {
                    return BasicOperationResult<IEnumerable<Post>>.Fail("Ha ocurrido un error al consultar las publicaciones.");
                }

                IEnumerable<Post> posts = JsonSerializer.Deserialize<IEnumerable<Post>>(postResponde.Content);

                return BasicOperationResult<IEnumerable<Post>>.Ok(posts);
            }
            catch (Exception ex)
            {
                return BasicOperationResult<IEnumerable<Post>>.Fail("Ha ocurrido un error al consultar las publicaciones.", ex.ToString());
            }
        }
    }
}
