﻿using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;
using RestSharp;
using System.Text.Json;
using System.Net;

namespace VisibleOutSource.Core.Managers
{
    public sealed class UserManager
    {
        private readonly IHttpServices _httpServices;
        const string urlHttpClient = "https://jsonplaceholder.typicode.com";
        const string urlObjectClient = "users";

        public UserManager(IHttpServices httpServices)
        {
            _httpServices = httpServices;
        }

        public async Task<IOperationResult<User>> GetDataUserById(int id)
        {
            try
            {
                var userResponde = await _httpServices.GetItem(urlHttpClient, urlObjectClient, id);

                if (!userResponde.IsSuccessful)
                {
                    if (userResponde.StatusCode == HttpStatusCode.NotFound)
                    {
                        return BasicOperationResult<User>.Fail("No existe el usuario con este Id.");
                    }
                    return BasicOperationResult<User>.Fail("Ha ocurrido un error al consultar el usuario.");
                }

                User user = JsonSerializer.Deserialize<User>(userResponde.Content.ToString());

                return BasicOperationResult<User>.Ok(user);

            }
            catch (Exception ex)
            {
                return BasicOperationResult<User>.Fail("Ha ocurrido un error al consultar el usuario.", ex.ToString());
            }
        }

        public async Task<IOperationResult<IEnumerable<User>>> GetDataAllUser()
        {
            try
            {
                var userResponde = await _httpServices.GetItems(urlHttpClient, urlObjectClient);

                if (!userResponde.IsSuccessful)
                {
                    return BasicOperationResult<IEnumerable<User>>.Fail("Ha ocurrido un error al consultar el usuario.");
                }

                IEnumerable<User> users = JsonSerializer.Deserialize<IEnumerable<User>>(userResponde.Content);

                return BasicOperationResult<IEnumerable<User>>.Ok(users);
            }
            catch (Exception ex)
            {
                return BasicOperationResult<IEnumerable<User>>.Fail("Ha ocurrido un error al consultar los usuarios.", ex.ToString());
            }
        }
    }
}
