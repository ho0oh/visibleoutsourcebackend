﻿using System.Text.Json;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Core.Managers
{
    public class PhotoManager
    {
        private readonly IHttpServices _httpServices;
        const string urlHttpClient = "https://jsonplaceholder.typicode.com";
        const string urlObjectClient = "photos";

        public PhotoManager(IHttpServices httpServices)
        {
            _httpServices = httpServices;
        }

        public async Task<IOperationResult<Photo>> GetDataPhotoById()
        {
            try
            {
                return BasicOperationResult<Photo>.Ok();
            }
            catch (Exception ex)
            {
                return BasicOperationResult<Photo>.Fail("Ha ocurrido un error al consultar la foto.", ex.ToString());
            }
        }

        public async Task<IOperationResult<IEnumerable<Photo>>> GetDataAllPhoto()
        {
            try
            {
                var photoResponde = await _httpServices.GetItems(urlHttpClient, urlObjectClient);

                if (!photoResponde.IsSuccessful)
                {
                    return BasicOperationResult<IEnumerable<Photo>>.Fail("Ha ocurrido un error al consultar las fotos.");
                }

                IEnumerable<Photo> photos = JsonSerializer.Deserialize<IEnumerable<Photo>>(photoResponde.Content);

                return BasicOperationResult<IEnumerable<Photo>>.Ok(photos);
            }
            catch (Exception ex)
            {
                return BasicOperationResult<IEnumerable<Photo>>.Fail("Ha ocurrido un error al consultar las fotos.", ex.ToString());
            }
        }
    }
}
