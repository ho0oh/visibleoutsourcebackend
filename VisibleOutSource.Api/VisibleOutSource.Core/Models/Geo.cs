﻿namespace VisibleOutSource.Core.Models
{
    public sealed class Geo
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }
}
