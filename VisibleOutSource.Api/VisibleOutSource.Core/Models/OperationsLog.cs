﻿
namespace VisibleOutSource.Core.Models
{
    public class OperationsLog
    {
        public int Id { get; set; }
        public string? ErrorMessage { get; set; }
        public string ErrorStackTrace { get; set; } = null!;
        public DateTime Date { get; set; }
    }
}
