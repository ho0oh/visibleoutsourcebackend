﻿namespace VisibleOutSource.Core.Models
{
    public sealed class Album
    {
        public int UserId { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
