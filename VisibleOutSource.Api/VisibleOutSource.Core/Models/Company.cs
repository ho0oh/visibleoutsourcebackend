﻿namespace VisibleOutSource.Core.Models
{
    public sealed class Company
    {
        public string name { get; set; }
        public string catchPhrase { get; set; }
        public string bs { get; set; }
    }
}
