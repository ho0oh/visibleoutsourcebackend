﻿using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Core.Interfaces
{
    public interface IOperationsLogRepository : IGenericRepository<OperationsLog>
    {
    }
}
