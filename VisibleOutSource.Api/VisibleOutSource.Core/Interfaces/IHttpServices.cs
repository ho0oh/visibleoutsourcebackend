﻿using RestSharp;

namespace VisibleOutSource.Core.Interfaces
{
    public interface IHttpServices
    {
        Task<RestResponse> GetItems(string urlClient, string urlObject);
        Task<RestResponse> GetItem(string httpUrlClient, string urlObject, int id);
    }
}
