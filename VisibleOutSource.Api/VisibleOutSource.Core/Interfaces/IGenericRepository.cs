﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace VisibleOutSource.Core.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        DbSet<T> Set { get; }
        IOperationResult<T> Create(T entity);
        Task<List<T>> FindAllAsync();
        Task SaveAsync();
    }
}
