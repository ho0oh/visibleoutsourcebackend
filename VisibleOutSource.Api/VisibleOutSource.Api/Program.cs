using Microsoft.EntityFrameworkCore;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Managers;
using VisibleOutSource.Persistence;
using VisibleOutSource.Persistence.Repositories;
using VisibleOutSource.Services.HttpService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IHttpServices, RestService>();
builder.Services.AddScoped<IOperationsLogRepository, OperationsLogRepository>();
//IGenericRepository

builder.Services.AddScoped<UserManager, UserManager>();
builder.Services.AddScoped<PhotoManager, PhotoManager>();
builder.Services.AddScoped<PostManager, PostManager>();
builder.Services.AddScoped<OperationsLogsManager, OperationsLogsManager>();

//builder.Services.AddCors(options =>
//{
//    options.AddDefaultPolicy(builder =>
//    {
//        builder.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader();
//    });
//});

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
    );

});


var connetioncString = builder.Configuration.GetConnectionString("DefaultConnectionString");
builder.Services.AddDbContext<ApplicationDbContext>(opciones =>
{
    opciones.UseSqlServer(connetioncString);
    opciones.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
}
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicy");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
