﻿using Microsoft.AspNetCore.Mvc;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Managers;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private readonly PhotoManager _photoManager;
        private readonly OperationsLogsManager _operationsLogsManager;

        public PhotoController(PhotoManager photoManager, OperationsLogsManager operationsLogsManager)
        {
            _photoManager = photoManager;
            _operationsLogsManager = operationsLogsManager;
        }

        [HttpGet("get-photo-by-id/{id}")]
        public async Task<IActionResult> GetPhotoById(int id)
        {
            IOperationResult<Photo> result = await _photoManager.GetDataPhotoById();

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPhotos()
        {
            IOperationResult<IEnumerable<Photo>> result = await _photoManager.GetDataAllPhoto();

            if (!result.Success)
            {
                await _operationsLogsManager.SaveLogError(result.Message, result.MessageDetail);
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }
    }
}
