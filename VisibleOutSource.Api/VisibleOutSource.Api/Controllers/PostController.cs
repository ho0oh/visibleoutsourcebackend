﻿using Microsoft.AspNetCore.Mvc;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Managers;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly PostManager _postManager;
        private readonly OperationsLogsManager _operationsLogsManager;

        public PostController(PostManager postManager, OperationsLogsManager operationsLogsManager)
        {
            _postManager = postManager;
            _operationsLogsManager = operationsLogsManager;
        }

        [HttpGet("get-post-by-id")]
        public async Task<IActionResult> GetPostById(int id)
        {
            IOperationResult<Post> result = await _postManager.GetDataPostById(id);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPosts()
        {
            IOperationResult<IEnumerable<Post>> result = await _postManager.GetDataAllPosts();

            if (!result.Success)
            {
                await _operationsLogsManager.SaveLogError(result.Message, result.MessageDetail);
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }
    }
}
