﻿using Microsoft.AspNetCore.Mvc;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Managers;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly OperationsLogsManager _operationsLogsManager;

        public LogController(OperationsLogsManager operationsLogsManager)
        {
            _operationsLogsManager = operationsLogsManager;
        }

        [HttpGet("DataLog")]
        public async Task<IActionResult> GetData()
        {
            IOperationResult<List<OperationsLog>> result = await _operationsLogsManager.GetData();

            if (!result.Success)
            {
                await _operationsLogsManager.SaveLogError(result.Message, result.MessageDetail);
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }
    }
}
