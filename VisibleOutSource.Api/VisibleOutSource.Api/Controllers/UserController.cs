﻿using Microsoft.AspNetCore.Mvc;
using VisibleOutSource.Core.Interfaces;
using VisibleOutSource.Core.Managers;
using VisibleOutSource.Core.Models;

namespace VisibleOutSource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager _userManager;
        private readonly OperationsLogsManager _operationsLogsManager;

        public UserController(UserManager userManager, OperationsLogsManager operationsLogsManager)
        {
            _userManager = userManager;
            _operationsLogsManager = operationsLogsManager; 
        }

        [HttpGet ("get-user-by-id")]
        public async Task<IActionResult> GetUserById(int id)
        {
            IOperationResult<User> result = await _userManager.GetDataUserById(id);

            if (!result.Success)
            {
                await _operationsLogsManager.SaveLogError(result.Message, result.MessageDetail);
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            IOperationResult<IEnumerable<User>> result = await _userManager.GetDataAllUser();

            if (!result.Success)
            {
                await _operationsLogsManager.SaveLogError(result.Message, result.MessageDetail);
                return BadRequest(result.Message);
            }

            return Ok(result.Entity);
        }
    }
}
