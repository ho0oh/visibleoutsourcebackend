﻿using RestSharp;
using VisibleOutSource.Core.Interfaces;

namespace VisibleOutSource.Services.HttpService
{
    public class RestService : IHttpServices
    {
        private string _urlClient;

        public async Task<RestResponse> GetItem(string urlClient, string urlObject, int id)
        {
            _urlClient = urlClient;
            var client = new RestClient(_urlClient);
            var request = new RestRequest($"/" + urlObject + "/" + id, Method.Get);
            var response = await client.ExecuteAsync(request);

            return response;
        }

        public async Task<RestResponse> GetItems(string urlClient, string urlObject)
        {
            _urlClient = urlClient;
            var client = new RestClient(_urlClient);
            var request = new RestRequest(urlObject, Method.Get);
            var response = await client.ExecuteAsync(request);

            return response;
        }
    }
}
